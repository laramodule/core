<?php

Route::group([
    'as' => 'laramodule::',
    'prefix' => '/admin',
    'middleware' => 'web',
    'namespace' => 'LaraModule\Core\Controllers',
], function () {
    // Home
    Route::get('/', function () {
        return 'Donkeys';
    })->name('index');

    // Login
    Route::group([
        'as' => 'login',
        'prefix' => '/login',
        'namespace' => 'Auth',
    ], function () {
        Route::get('/', 'LoginController@showLoginForm');
        Route::post('/', 'LoginController@login');
    });
});
