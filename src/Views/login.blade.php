@extends ('laramodule::layout')

@section ('body')
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-4 col-md-offset-4">
        <div class="pega-header">
          <h3 class="page-title">Fazer Login</h3>
        </div>

        {!! Form::open() !!}
          {!! Form::email('E-mail', 'email')->placeholder('E-mail') !!}
          {!! Form::password('Senha', 'password')->placeholder('Senha') !!}
          {!! Form::submit('Entrar', 'btn-primary') !!}
          <a href="#" class="pull-right">Esqueceu sua senha?</a>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
@endsection
