<?php

namespace LaraModule\Core\Controllers\Auth;

use LaraModule\Core\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use LaraModule\Core\Middleware\RedirectIfAuthenticated;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * {@inheritdoc}
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(RedirectIfAuthenticated::class, ['except' => 'logout']);
    }

    /**
     * {@inheritdoc}
     */
    public function showLoginForm()
    {
        return view('laramodule::login');
    }
}
