<?php

namespace LaraModule\Core;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use AdamWathan\BootForms\Facades\BootForm;
use AdamWathan\BootForms\BootFormsServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__.'/routes.php';
        }

        $this->loadViewsFrom(__DIR__.'/Views', 'laramodule');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(BootFormsServiceProvider::class);

        $loader = AliasLoader::getInstance();
        $loader->alias('Form', BootForm::class);
    }
}
