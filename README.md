# Core

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

Build modular applications using LaraModule.

## Install

Via Composer

``` bash
$ composer require laramodule/core
```

## Usage

Coming soon...

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CONDUCT](CONDUCT.md) for details.

## Security

If you discover any security related issues, please email lucas.pires.mattos@gmail.com instead of using the issue tracker.

## Credits

- [Lucas Pires][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/laramodule/core.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/laramodule/core/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/66746426/shield
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/laramodule/core.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/laramodule/core.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/laramodule/core.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/laramodule/core
[link-travis]: https://travis-ci.org/laramodule/core
[link-styleci]: https://styleci.io/repos/66746426
[link-scrutinizer]: https://scrutinizer-ci.com/g/laramodule/core/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/laramodule/core
[link-downloads]: https://packagist.org/packages/laramodule/core
[link-author]: https://github.com/flyingluscas
[link-contributors]: ../../contributors
