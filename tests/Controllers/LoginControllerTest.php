<?php

namespace LaraModule\Core\Tests\Controllers;

use Illuminate\Support\Facades\DB;
use LaraModule\Core\Tests\TestCase;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginControllerTest extends TestCase
{
    protected $user;

    protected $email;

    protected $password;

    public function setUp()
    {
        parent::setUp();

        $this->email = 'donkey@mydonkey.com';
        $this->password = 'donkeysDontFly';

        $this->user = DB::table('users')->insertGetId([
            'name' => 'Donkey',
            'email' => $this->email,
            'password' => Hash::make($this->password),
        ]);
    }

    public function testLogin()
    {
        $this->post('/admin/login', [
            'email' => $this->email,
            'password' => $this->password,
        ]);

        $this->assertRedirectedTo('/admin');
        $this->assertTrue(Auth::check());
    }

    public function testRedirectIfAuthenticated()
    {
        $this->actingAs(User::find($this->user))
             ->visit('/admin/login')
             ->seePageIs('/admin');
    }
}
