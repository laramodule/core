<?php

namespace LaraModule\Core\Tests;

use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Hash;
use LaraModule\Core\CoreServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

abstract class TestCase extends OrchestraTestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->setUpDatabaseMigrations();
    }

    /**
     * {@inheritdoc}
     */
    protected function getPackageProviders($app)
    {
        return [CoreServiceProvider::class];
    }

    /**
     * {@inheritdoc}
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);

        $app['config']->set('auth.providers.users.model', User::class);

        Hash::setRounds(5);
    }

    /**
     * Run database migrations.
     *
     * @return void
     */
    protected function setUpDatabaseMigrations()
    {
        $this->loadMigrationsFrom(realpath(__DIR__.'/Migrations'));
    }
}
