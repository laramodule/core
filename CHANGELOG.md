# Changelog

All Notable changes to `laramodule/core` will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [Unreleased]

### Added
- Recover Password
- Manage Account

## v0.1.0 - 2016-08-29

### Added
- Login
